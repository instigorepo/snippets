package gd.Speedometer;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.util.Log;

public class Speedometer extends Activity {

    private TextView tv;
    private LocationManager lm;
    private LocationListener ll;
    double mySpeed, maxSpeed;
    private final String Speed = null;
   
    /* This requires adding: android.permission.ACCESS_FINE_LOCATION
       within the Manifest file */

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(Speed, "working1 ");
        super.onCreate(savedInstanceState);
        tv = new TextView(this);
        setContentView(tv);

        maxSpeed = mySpeed = 0;
        Log.i(Speed, "working1 ");
        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        ll = new SpeedoActionListener();
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,ll);
    }

    private class SpeedoActionListener implements LocationListener
    {

        @Override
        public void onLocationChanged(Location location) {
            Log.i(Speed, "working2 ");
            if(location!=null) {
                if(location.hasSpeed()){
                    mySpeed = location.getSpeed();
                    tv.setText("\nCurrent speed: " + mySpeed 
                        + " km/h, Max speed: " + maxSpeed + " km/h");
                }
            }
            Log.i(Speed, "working3 ");
        }

        @Override
        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub
                
	}

        @Override
        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // TODO Auto-generated method stub

        }
    }
}
